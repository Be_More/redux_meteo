module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
        "linebreak-style": [
            "error",
            "unix"
        ],
        "semi": [
            "error",
            "never"
        ],
		
		"no-debugger":	0,
		"no-console":	0,
		"new-cap":	0,
		"strict":	0,
		"no-underscore-dangle":	0,
		"no-use-before-define":	0,
		"eol-last":	0,
		"quotes":	[1,	"single"],
		"jsx-quotes":	[1,	"prefer-single"],
		"react/jsx-no-undef":	1,
		"react/jsx-uses-react":	1,
		"react/jsx-uses-vars":	1,
		"no-extra-semi": 0,
		"no-undef": 1,
		"no-unused-vars": 1,
		"no-mixed-spaces-and-tabs": 0,
		"no-unreachable": 1
    }
};