import React, { Component } from 'react'
import PropTypes from 'prop-types'
import '../styles/Search.css'
import SearchResult from '../components/SearchResult'

export default class Search extends Component {

    onSearchClick(e) {
        this.props.search(this.searchQuery.value)
    }

    onClearSearch() {
        this.searchQuery.value = null
        this.props.clear()
    }

    searchFocus(){
        this.searchQuery.focus()
    }

    render() {

        let searchResult

        if (this.props.fetching === 1) {

            searchResult = (
            <div className={'fetching_true'}>
                <div className='preloader-wrapper medium active'>
                    <div className='spinner-layer spinner-blue'>
                        <div className='circle-clipper left'>
                            <div className='circle'></div>
                        </div>
                        <div className='gap-patch'>
                            <div className='circle'></div>
                        </div>
                        <div className='circle-clipper right'>
                            <div className='circle'></div>
                        </div>
                    </div>

                    <div className='spinner-layer spinner-red'>
                        <div className='circle-clipper left'>
                            <div className='circle'></div>
                        </div>
                        <div className='gap-patch'>
                            <div className='circle'></div>
                        </div>
                        <div className='circle-clipper right'>
                            <div className='circle'></div>
                        </div>
                    </div>

                    <div className='spinner-layer spinner-yellow'>
                        <div className='circle-clipper left'>
                            <div className='circle'></div>
                        </div>
                        <div className='gap-patch'>
                            <div className='circle'></div>
                        </div>
                        <div className='circle-clipper right'>
                            <div className='circle'></div>
                        </div>
                    </div>

                    <div className='spinner-layer spinner-green'>
                        <div className='circle-clipper left'>
                            <div className='circle'></div>
                        </div>
                        <div className='gap-patch'>
                            <div className='circle'></div>
                        </div>
                        <div className='circle-clipper right'>
                            <div className='circle'></div>
                        </div>
                    </div>
                </div>
            </div>
            )
        }

        if (this.props.fetching === 2) {

            let searchedCity = this.props.searchResult
            let changeCity = this.props.changeCity

            searchResult = searchedCity.map(function (item, index) {
                return (
                    <div key={index}>
                        <SearchResult
                            id={item.id}
                            name={item.name}
                            country={item.sys.country}
                            temperature={Math.floor(item.main.temp-273)}
                            changeCity={changeCity}
                        />
                    </div>
                )
            })

        }

        return <div className='search'>
            <div className='input-field col s6'>
                {/*<form>*/}
                <input type='text'
                       className='search__input'
                       ref={(input) => {
                           this.searchQuery = input
                       }}
                       onChange={this.onSearchClick.bind(this)}
                />
                <label onClick={this.searchFocus.bind(this)}>
                    Поиск
                </label>
                <button
                    className={ 'search__clear' }
                    onClick={this.onClearSearch.bind(this)}>
                    X
                </button>
            </div>
            <div className='search-result-box'>
                {searchResult}
            </div>
        </div>

    }

    // Сделать букмарки.




}



Search.propTypes = {
    search: PropTypes.func.isRequired,
    clear: PropTypes.func.isRequired,
    changeCity: PropTypes.func.isRequired
}
