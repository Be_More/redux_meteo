import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withCookies } from 'react-cookie'
import logo from '../logo.svg'
import Weather from './Weather'
import Favorites from './Favorites'
import '../styles/App.css'
import bindActionCreators from 'redux/src/bindActionCreators'
import * as weatherActions from '../actions/WeatherActions'
import * as favoritesActions from '../actions/FavoritesActions'

class App extends Component {
    render() {
        const { weather, forecast, searchResult } = this.props
        const weatherActions = this.props.weatherActions
        const favoritesActions = this.props.favoritesActions

        return (
            <div className='App'>
                <header className='App-header'>
                    <img src={logo} className='App-logo' alt='logo' />
                    <h1 className='App-title'>Welcome to React</h1>
                </header>
                <div className='meteo-app'>
                    <Weather
                        CurrentWeather={weather}
                        weatherActions={weatherActions}
                        Forecast={forecast}
                    />
                    <Favorites
                        favoritesActions={favoritesActions}
                        Favorites={searchResult}
                    />
                </div>
            </div>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        cookies: ownProps.cookies,
        weather: state.weather,
        forecast: state.forecast,
        searchResult: state.searchResult
    }
}

function mapDispatchToProps(dispatch) {
    return {
        weatherActions: bindActionCreators(weatherActions, dispatch),
        favoritesActions: bindActionCreators(favoritesActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
//export default App;

//Доделать куки https://medium.com/@rossbulat/using-cookies-in-react-redux-and-react-router-4-f5f6079905dc
// http://qaru.site/questions/840040/redux-what-is-the-correct-place-to-save-cookie-after-login-request