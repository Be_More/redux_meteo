import React, { Component } from 'react'
import Search from './Search'
import Cities from './Cities'

export default class Favorites extends Component {

    render() {

        const { fetching, searchResult }= this.props.Favorites
        const { search, clear, changeCity } = this.props.favoritesActions
        // const favorites = this.props.favorites
        // const { searchResult} = this.props.searchResult

        return <div className='favorites'>
            <Search
                search={search}
                clear={clear}
                fetching={fetching}
                searchResult={searchResult}
                changeCity={changeCity}
            />

            <Cities
                // cities={cities}
            />

        </div>
    }

}