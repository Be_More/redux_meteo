import React, { Component } from 'react'
import Forecast from './Forecast'
import CurrentWeather from '../components/CurrentWeather'
import '../styles/Weather.css'


export default class Weather extends Component {

    render() {
        const { id, name, temperature, cloud, wind, icon, fetching } = this.props.CurrentWeather
        const forecast = this.props.Forecast.dayForecasts
        const timezone = this.props.Forecast.timezone
        const lat = this.props.Forecast.lat
        const lon = this.props.Forecast.lon
        const { getWeather, getForecast } = this.props.weatherActions


        return <div className='weather'>

            <CurrentWeather
                id={id}
                name={name}
                getWeather={getWeather}
                fetching={fetching}
                temperature={temperature}
                wind={wind}
                icon={icon}
                cloud={cloud}
            />

            <Forecast
                id={id}
                name={name}
                lat={lat}
                lon={lon}
                timezone={timezone}
                dayForecasts={forecast}
                getForecast={getForecast}
            />
        </div>
    }
}

