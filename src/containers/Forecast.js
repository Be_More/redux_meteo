import React, { Component } from 'react'
import PropTypes from 'prop-types'
import '../styles/Forecast.css'
import DayForecast from '../components/DayForecast'

export default class Forecast extends Component {

    componentDidMount() {
        this.props.getForecast(this.props.id)
    }

    componentWillReceiveProps(nextProps){
        if(this.props.id !== nextProps.id){
            this.props.getForecast(nextProps.id)
        }
    }

    render(){

        let days   //измерения погоды
        let requiredDays = []
        let dayTemplate  //шаблон для прогноза на 1 день
        let forecastDelta  //смещение относильно Гринфича (в измерениях, а не в часах. 1 измерение = 3 часа)
        let firstNight  //идекс первого значимого измерения
        let totalDays  //количество дней с прогнозом
        const weekdays=["Сб", "Вс", "Пн", "Вт", "Ср", "Чт", "Пт"]

        if ((this.props.dayForecasts.length > 0) && (this.props.timezone < 25)){

            days = this.props.dayForecasts

            forecastDelta = Math.floor(this.props.timezone / 3)

            /* 8ая отметка времени (полночь, цель) - текущее время + смещение по часовому поясу*/
            firstNight =  8 - (new Date(days[0].dt_txt)).getHours() / 3 - forecastDelta
            if(firstNight < 0) {
                firstNight += 8
            }
            if(firstNight > 7) {
                firstNight -= 8
            }

            /*количество измерений + округление до полуночи последнего дня - смещение на измерение первого дня
            делить на колличество измерений за сутки*/
            totalDays = Math.floor((days.length + 4 - firstNight) / 8)

            /*сместили первое значимое измерение на 0ю позицию*/
            days.splice(0,firstNight)

            for(let i = 0; i < totalDays; i ++) {
                requiredDays[i] = {
                    temperatureNight: Math.floor(days[i * 8].main.temp - 273),
                    temperatureDay: Math.floor(days[i * 8 + 4].main.temp - 273),
                    weekday: weekdays[new Date(days[i * 8 + 4].dt_txt).getDay()]
                }
            }

            dayTemplate = requiredDays.map(function(item, index) {
                return (
                    <div key={index}>
                        <DayForecast
                            temperatureDay={item.temperatureDay}
                            temperatureNight={item.temperatureNight}
                            weekday={item.weekday}
                        />
                    </div>
                )
            })
        }
        else{
            dayTemplate=<p>Прогноза не будет</p>
        }

        return (
            <div className='forecast'>
                {dayTemplate}
            </div>
        )
    }

}



Forecast.propTypes = {
    // dayForecasts: PropTypes.array().isRequired(),
    getForecast: PropTypes.func.isRequired
}