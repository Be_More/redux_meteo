import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import {Provider} from 'react-redux'
import App from './containers/App'
import registerServiceWorker from './registerServiceWorker'
import configureStore from './store/configureStore'
import { CookiesProvider } from 'react-cookie'

const store = configureStore()

ReactDOM.render(
    <CookiesProvider>
        <Provider store={store}>
            <div className='app'>
                <App />
            </div>
        </Provider>
    </CookiesProvider>,
    document.getElementById('root')
)

registerServiceWorker()
