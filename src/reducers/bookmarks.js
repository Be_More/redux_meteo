import * as weatherConsts from '../constants/Weather'

const initialState = {
    savedCities:[]
}



export default function bookmarks(state = initialState, action){

    switch(action.type){

        case weatherConsts.GET_WEATHER_REQUEST:
            return {
                ...state,
                fetching: true
            }

        case weatherConsts.GET_WEATHER_SUCCESS:
            return {
                ...state,
                city: action.payload.data.name,
                fetching: false
            }

        case weatherConsts.CHANGE_CITY_SUCCESS:
            return {
                ...state,
                id: action.payload,
                fetching: false
            }

        default:
            return state
    }

}