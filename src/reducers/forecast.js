import * as weatherConsts from '../constants/Weather'

const initialState = {
    city: 'Tomsk',
    lat: 0,
    lon: 0,
    timezone: 25,
    dayForecasts: []

}



export default function forecast(state = initialState, action){

    switch(action.type){

        case weatherConsts.GET_FORECAST_REQUEST:
            return {
                ...state
            }

        case weatherConsts.GET_FORECAST_SUCCESS:
            return {
                ...state,
                dayForecasts: action.payload.data.list
            }

        case weatherConsts.GET_TIMEZONE_SUCCESS:
            return {
                ...state,
                timezone: action.payload.data.rawOffset
            }

        default:
            return state
    }


}