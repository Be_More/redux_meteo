import * as weatherConsts from '../constants/Weather'

const initialState = {
    name: 'Tomsk',
    id: 1489425,
    temperature: 0,
    fetching: false
}



export default function weather(state = initialState, action){
    let temperature

    switch(action.type){

        case weatherConsts.GET_WEATHER_REQUEST:
            return {
                ...state,
                fetching: true
            }

        case weatherConsts.GET_WEATHER_SUCCESS:
            temperature = Math.floor(action.payload.data.main.temp - 273)
            return {
                ...state,
                temperature: temperature,
                name: action.payload.data.name,
                fetching: false
            }

        case weatherConsts.CHANGE_CITY_SUCCESS:
            return {
                ...state,
                id: action.payload,
                fetching: false
            }

        default:
            return state
    }

}