import * as weatherConsts from '../constants/Weather'

const initialState = {
    searchResult:[],
    fetching: 0
}

export default function search(state = initialState, action){

    switch(action.type){

        case weatherConsts.GET_SEARCH_REQUEST:
            return {
                ...state,
                // name: action.payload,
                fetching: 1
            }

        case weatherConsts.GET_SEARCH_SUCCESS:
            return {
                ...state,
                searchResult: action.payload.data.list,
                fetching: 2
            }


        case weatherConsts.CLEAR_SEARCH_SUCCESS:
            return {
                ...state,
                searchResult: [],
                fetching: 2
            }

        default:
            return state
    }

}