import { combineReducers } from 'redux'
import weather from './curentWeather'
import forecast from './forecast'
import searchResult from './search'
import bookmarks from './bookmarks'

export default combineReducers({
    weather,
    forecast,
    searchResult,
    bookmarks
})