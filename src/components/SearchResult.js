import React, { Component } from 'react'
import '../styles/Weather.css'

export default class SearchResult extends Component {

    onCityClick() {
        this.props.changeCity(this.props.id)
    }

    render() {
        const { name, country, temperature} = this.props

        return <p className='search-result'
                  onClick={this.onCityClick.bind(this)}>
            <span className='search-result__name'>{name}, {country}</span>
            <span className='search-result__temp'>  {(temperature > 0 ? '+' + temperature : temperature)}
                <span className='search__degrees'> o</span>C
            </span>
        </p>
    }
}


