import React, { Component } from 'react'
import PropTypes from 'prop-types'
import '../styles/Weather.css'

export default class CurrentWeather extends Component {

    addToFav() {

    }

    componentDidMount() {
        this.props.getWeather(this.props.id)
    }

    componentWillReceiveProps(nextProps){
        if(this.props.id !== nextProps.id){
            this.props.getWeather(nextProps.id)
        }
    }

    render() {
        const { name, temperature,  fetching} = this.props

        return <div className='weather'>
            <p className='weather__title'>{name}</p>

            {/*Крутилка*/}
            <div className={'fetching_'+fetching}>
                <div className="preloader-wrapper medium active">
                    <div className="spinner-layer spinner-blue">
                        <div className="circle-clipper left">
                            <div className="circle"></div>
                        </div>
                        <div className="gap-patch">
                            <div className="circle"></div>
                        </div>
                        <div className="circle-clipper right">
                            <div className="circle"></div>
                        </div>
                    </div>

                    <div className="spinner-layer spinner-red">
                        <div className="circle-clipper left">
                            <div className="circle"></div>
                        </div>
                        <div className="gap-patch">
                            <div className="circle"></div>
                        </div>
                        <div className="circle-clipper right">
                            <div className="circle"></div>
                        </div>
                    </div>

                    <div className="spinner-layer spinner-yellow">
                        <div className="circle-clipper left">
                            <div className="circle"></div>
                        </div>
                        <div className="gap-patch">
                            <div className="circle"></div>
                        </div>
                        <div className="circle-clipper right">
                            <div className="circle"></div>
                        </div>
                    </div>

                    <div className="spinner-layer spinner-green">
                        <div className="circle-clipper left">
                            <div className="circle"></div>
                        </div>
                        <div className="gap-patch">
                            <div className="circle"></div>
                        </div>
                        <div className="circle-clipper right">
                            <div className="circle"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div className={'weather__current-state ' + fetching}>
                <div className='weather__current-weather'>
                    <div className='weather__cloudiness-temp'>
                        <p className='weather__temperature'>
                            <span className='weather__celsius'>{temperature}
                                <span className='weather__degrees'>o</span>C
                            </span>
                        </p>
                    </div>
                </div>
            </div>

            <button
                className="waves-effect waves-light btn"
                onClick={this.addToFav.bind(this)}>
                Добавить в избранное
            </button>

        </div>
    }
}


CurrentWeather.propTypes = {
    name: PropTypes.string.isRequired,
    getWeather: PropTypes.func.isRequired,
    fetching: PropTypes.bool.isRequired,
    temperature: PropTypes.number.isRequired
}