import React, { Component } from 'react'
import PropTypes from 'prop-types'
import '../styles/Weather.css'

export default class DayForecast extends Component {


    render() {
        const { temperatureDay, temperatureNight, weekday } = this.props

        return <div className='weather-forecast'>
            <div className='weather-forecast__daily-forecast'>
                <p className='weather-forecast__weekday weather-forecast__weekday_today'>{weekday}</p>
                <div className='weather-forecast__temperatures'>
                    <p className='weather-forecast__temperature temperature_day'>
                        <span className='weather-forecast__celsius'>{temperatureDay}
                        <span className='weather-forecast__degrees'>o</span>C
                        </span>
                    </p>
                    <p className='weather-forecast__temperature temperature_night'>
                        <span className='weather-forecast__celsius'>{temperatureNight}
                        <span className='weather-forecast__degrees'>o</span>C
                        </span>
                    </p>
                </div>
            </div>
        </div>
    }
}


DayForecast.propTypes = {
    temperatureDay: PropTypes.number.isRequired,
    temperatureNight: PropTypes.number.isRequired,
    weekday: PropTypes.string.isRequired
}