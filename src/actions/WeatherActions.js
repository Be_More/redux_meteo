import axios from 'axios'
import * as weatherConsts from '../constants/Weather'
import * as apiConst from '../constants/API'

export function getWeather(id){

    return (dispatch) => {

        dispatch({
            type: weatherConsts.GET_WEATHER_REQUEST,
            payload: 'Loading...'
        })

        axios.get(apiConst.PROXY + apiConst.API_WEATHER_URL + '/weather',{
            params: {
                appid: apiConst.API_KEY,
                id: id
            }
        })
            .then((res) => {
                dispatch({
                    type: weatherConsts.GET_WEATHER_SUCCESS,
                    payload: res
                })
            })
    }

}

export function getForecast(id){

    return (dispatch) => {

        dispatch({
            type: weatherConsts.GET_FORECAST_REQUEST,
            payload: 'Loading...'
        })

        axios.get(apiConst.PROXY + apiConst.API_WEATHER_URL + '/forecast',{
            params: {
                appid: apiConst.API_KEY,
                id: id
            }
        })
            .then(res => {
                dispatch({
                    type: weatherConsts.GET_FORECAST_SUCCESS,
                    payload: res
                })



                axios.get(apiConst.PROXY + apiConst.API_TIMEZONE_URL,{
                    params: {
                        formatted: true,
                        lat: res.data.city.coord.lat,
                        lng: res.data.city.coord.lon,
                        username: apiConst.API_TIMEZONE_USERNAME
                    }
                })
                    .then(res =>{
                        dispatch({
                            type: weatherConsts.GET_TIMEZONE_SUCCESS,
                            payload: res
                        })
                    })
            })

    }

}


