import axios from 'axios'
import * as weatherConsts from '../constants/Weather'
import * as apiConst from '../constants/API'

export function search(city){

    return (dispatch) => {

        dispatch({
            type: weatherConsts.GET_SEARCH_REQUEST,
            payload: 'Loading...'
        })

        axios.get(apiConst.PROXY + apiConst.API_WEATHER_URL + '/find',{
            params: {
                appid: apiConst.API_KEY,
                q: city
            }
        })
            .then((res) => {
                dispatch({
                    type: weatherConsts.GET_SEARCH_SUCCESS,
                    payload: res
                })
            })
    }

}

export function clear(){

    return (dispatch) => {

        dispatch({
            type: weatherConsts.CLEAR_SEARCH_SUCCESS,
            payload: ''
        })
    }
}

export function changeCity(id){

    return (dispatch) => {

        dispatch({
            type: weatherConsts.CHANGE_CITY_SUCCESS,
            payload: id
        })
    }
}